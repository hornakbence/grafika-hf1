#ifndef CURVE_HPP
#define CURVE_HPP

struct Curve {

    virtual ~Curve() {
    }
    virtual vec2 r(GLfloat t) const = 0;

    virtual void genCoords(int num, GLfloat* coords) const {
        for (int i = 0; i < num; ++i) {
            vec2 p = r((GLfloat) i / (num - 1));
            coords[2 * i + 0] = p.x;
            coords[2 * i + 1] = p.y;
        }
    }

};

#endif /* CURVE_HPP */

