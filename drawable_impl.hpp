#ifndef DRAWABLE_IMPL_HPP
#define DRAWABLE_IMPL_HPP

template<int numPoints>
void Drawable<numPoints>::useProgram() {
    world.useGeneralShader();
}

#endif /* DRAWABLE_IMPL_HPP */

