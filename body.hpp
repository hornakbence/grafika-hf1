#ifndef BODY_HPP
#define BODY_HPP



struct Body : public ButterflyDrawable<BODY_POINTS> {
private:
    Ellipse ellipse;

    void genPoints() {
        ellipse.genCoords(BODY_POINTS, coords);

        for (int i = 0; i < BODY_POINTS; i++) {
            // #1d102e
            colors[3 * i + 0] = 29.0 / 255;
            colors[3 * i + 1] = 16.0 / 255;
            colors[3 * i + 2] = 46.0 / 255;
        }

    }

public:

    Body(const Butterfly& butterfly) : ButterflyDrawable(butterfly), ellipse(0, -.1, .1, .7) {
        genPoints();
    }

    mat4 transform2() override {
        return mat4::identity();
    }


};

#endif /* BODY_HPP */

