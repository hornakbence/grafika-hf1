#ifndef BUTTERFLY_HPP
#define BUTTERFLY_HPP

struct Wing;
struct Body;

struct Butterfly {
private:

    Wing *leftWing, *rightWing;
    Body *body;

    /** Position */
    vec4 r;
    /** Velocity */
    vec4 v;
    /** Acceleration */
    vec4 a;

    /** 
     * Normalized vectors. i: right, j forward, k = i × j
     */
    vec4 i, j, k;

    void create();
    void destroy();
    void updatePhysics(GLfloat dt, bool mouseDown, vec2 mousePos);

    static vec4 gravity;
public:

    Butterfly();
    ~Butterfly();
    void init();
    void update(GLfloat t, GLfloat dt, bool mouseDown, const vec2& mousePos = vec2());
    void draw();
    mat4 mvp() const;
};

#endif /* BUTTERFLY_HPP */

