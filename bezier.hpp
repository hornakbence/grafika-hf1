#ifndef BEZIER_HPP
#define BEZIER_HPP

struct Bezier : public Curve {
private:
    const std::vector<vec2> controlPoints;

    float weight(int i, float t) const {
        int n = controlPoints.size();
        float bernstein = 1;
        for (int j = 1; j <= i; ++j)
            bernstein *= (float) (n - j + 1) / j;
        bernstein *= pow(t, i) * pow(1 - t, n - i);
        return bernstein;
    }
public:

    Bezier(const std::vector<vec2> controlPoints)
    : controlPoints(controlPoints) {
    }

    vec2 r(GLfloat t) const {
        vec2 out;
        for (unsigned i = 0; i < controlPoints.size(); i++) {
            out += weight(i, t) * controlPoints[i];
        }
        return out;
    }
};

#endif /* BEZIER_HPP */

