#ifndef BUTTERFLY_DRAWABLE_IMPL_HPP
#define BUTTERFLY_DRAWABLE_IMPL_HPP

template<int numPoints>
void ButterflyDrawable<numPoints>::useProgram() {
    world.useButterflyShader();
}

#endif /* BUTTERFLY_DRAWABLE_IMPL_HPP */

