#ifndef FLOWER_CURVE_HPP
#define FLOWER_CURVE_HPP

struct FlowerCurve : public Curve {
private:
    int numPetals;

public:

    FlowerCurve(int numPetals) : numPetals(numPetals) {
    }

    vec2 r(GLfloat t) const override {
        GLfloat len = .7 + .3 * sinf(numPetals * (2 * M_PI) * t);
        GLfloat angle = 2 * M_PI * t;
        return vec2(len * cosf(angle), len * sinf(angle));
    }

    void genCoords(int num, GLfloat* coords) const override {
        coords[0] = coords[1] = 0;
        for (int i = 0; i < num - 1; i++) {
            vec2 p = r((GLfloat) i / (num - 2));
            coords[2 * (1 + i) + 0] = p.x;
            coords[2 * (1 + i) + 1] = p.y;
        }

    }

};

#endif /* FLOWER_CURVE_HPP */

