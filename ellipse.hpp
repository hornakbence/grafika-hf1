#ifndef ELLIPSE_HPP
#define ELLIPSE_HPP

struct Ellipse : public Curve {
private:
    GLfloat cx, cy, width, height;
public:

    Ellipse(GLfloat cx, GLfloat cy, GLfloat width, GLfloat height)
    : cx(cx), cy(cy), width(width), height(height) {
    }

    vec2 r(GLfloat t) const {
        return vec2(
                cx + cosf(t * (2 * M_PI)) * width / 2,
                cy + sinf(t * (2 * M_PI)) * height / 2
                );
    }

};

#endif /* ELLIPSE_HPP */

