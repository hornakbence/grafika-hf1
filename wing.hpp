#ifndef WING_HPP
#define WING_HPP

struct Wing : public ButterflyDrawable<WING_CURVE_POINTS> {
private:
    Bezier curve1, curve2;
    bool left;

    void genPoints() {

        curve1.genCoords(WING_CURVE1_POINTS, coords);
        curve2.genCoords(WING_CURVE2_POINTS, coords + 2 * WING_CURVE1_POINTS);

        // #3A204C
        colors[0] = .23;
        colors[1] = .13;
        colors[2] = .30;

        for (int i = 1; i < WING_CURVE_POINTS; i++) {

            colors[3 * i + 0] = .30;
            colors[3 * i + 1] = .16;
            colors[3 * i + 2] = .8;
        }

    }


public:

    Wing(const Butterfly& butterfly, bool flip) :
    ButterflyDrawable<WING_CURVE_POINTS>(butterfly),
    curve1({
        vec2(.3, .2), vec2(.7, -.1), vec2(.8, -.2), vec2(.1, -.7),
        vec2(0, -.9), vec2(0, 0)
    }), curve2({
        vec2(0, 0), vec2(.2, .4), vec2(.5, .8), vec2(.8, .8), vec2(.8, .7),
        vec2(.8, .4), vec2(.6, -.2), vec2(.6, 0),
        vec2(.5, 0), vec2(.3, -.2)
    }), left(flip) {
        genPoints();
    }

    mat4 mvp() override {
        mat4 translate = mat4::translate(.03, 0);
        mat4 flip = left ? mat4::scale(-1, 1) : mat4::identity();
        mat4 scale = mat4::scale(1.29 - 1 / (2.5 + cosf(3 * t)), 1);
        return scale * translate * flip * ButterflyDrawable<WING_CURVE_POINTS>::mvp();
    }
    
    mat4 transform2() override {
        return left ? mat4::scale(-1, 1) : mat4::identity();
    }


};

#endif /* WING_HPP */

